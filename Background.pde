class Background{
  String image_path = "background/layer_background.png";
  
  PImage background;
  
  Background(){
    background = loadImage(image_path);
  }
  
  void drawing(){
    image(background, 0, 0, width, height);
  }
}